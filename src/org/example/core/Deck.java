package org.example.core;

import java.util.ArrayList;

/**
 * Initialize a deck and deal cards to players.
 */
public class Deck {
    
    private ArrayList<Card> deck; 
    private ArrayList<Card> delt;
    static int SUITE_SIZE = 13;

    public Deck() {

        this.deck = new ArrayList<>();
        this.delt = new ArrayList<>();

        /* Initialize deck with the corresponding number of cards for each suite */
        for(int i = 1 ; i <= SUITE_SIZE; i++) {
            this.deck.add(new Card(i, Suite.CLUBS));
            this.deck.add(new Card(i, Suite.DIAMONDS));
            this.deck.add(new Card(i, Suite.HEARTS));
            this.deck.add(new Card(i, Suite.SPADE));
        }
    }

    /**
     * Return a card from the deck. If no more cards available,
     * return null.
     * @return currentCard (Card) or null
     */
    public Card dealCard() {

        Card currentCard = null;
        if (!this.deck.isEmpty()){
            int rand = (int)(Math.random() * ((this.deck.size() - 1)));
            currentCard = this.deck.get(rand);
            this.deck.remove(currentCard);
            this.delt.add(currentCard);
        }

        return currentCard;
    }
}
