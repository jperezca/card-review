package org.example.core;

/**
 * Define a card with a given suite and number.
 */
public class Card {

    private String suite;
    private int value;

    public Card(int value, String suite) {
        this.suite = suite;
        this.value = value;
    }

    @Override
    public String toString() {
        String cardString = String.format("Card{suite='%s', value=%d}", this.suite, this.value);
        return cardString;
    }

    public String getSuite() {
        return this.suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
