package org.example.core;

/**
 * Define the four suites available at a Deck
 */
public class Suite {

    public static String HEARTS = "H";
    public static String SPADE = "S";
    public static String CLUBS = "C";
    public static String DIAMONDS = "D";

}