package org.example.core;

import java.util.ArrayList;

/**
 * Create a player with an empty hand
 */
public class Player {

    private ArrayList<Card> hand;

    public Player() {
        this.hand = new ArrayList<>();
    }

    public void showHand() {
        System.out.println(this.hand);
    }

    public ArrayList<Card> getHand() {
        return this.hand;
    }

    public void setHand(ArrayList<Card> hand) {
        this.hand = hand;
    }

}
