package org.example.core;

/**
 * Singleton to manage a Game
 */
public class Game implements AutoCloseable {

    private static Game instance;
    private Deck deck;

    private Game() {
        this.deck = new Deck();
    }

    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    @Override
    public void close() throws Exception {
        System.out.println("Cleaning up instance");
        instance = null;
    }

    /**
     * Grab a card from the deck and deal it to a player.
     * If no cards available, terminate the game.
     * @param p Player
     */
    public void dealCard(Player p) {

        Card currentCard = this.deck.dealCard();

        if (currentCard == null){
            try {
                this.close();
                System.out.println("Game finished");
            }
            catch (Exception e) {
                System.out.println("Failed to terminate game");
            }
        }
        else {
            p.getHand().add(this.deck.dealCard());
        }
    }
}
