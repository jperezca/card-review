package org.example;

import org.example.core.Game;
import org.example.core.Player;

/**
 * The App class initializes two players and starts a game, printing
 * both player hands at the end.
 */
public class App {
    public static void main(String[] args) {

        int numCards = 5;
        Player p1 = new Player();
        Player p2 = new Player();
        Game game = Game.getInstance();

        /* Deal 5 cards to each player */
        for (int i = 0; i < numCards; i++) {
            game.dealCard(p1);
            game.dealCard(p2);
        }

        /* Show players hands */
        p1.showHand();
        p2.showHand();
    }
}
